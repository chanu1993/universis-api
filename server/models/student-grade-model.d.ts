import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import CourseExam = require('./course-exam-model');
import Student = require('./student-model');
import CourseClass = require('./course-class-model');
import Course = require('./course-model');

/**
 * @class
 */
declare class StudentGrade extends DataObject {

     
     /**
      * @description Id
      */
     public id: string; 
     
     /**
      * @description Course Exam
      */
     public courseExam: CourseExam|any; 
     
     /**
      * @description Student
      */
     public student: Student|any; 
     
     /**
      * @description Class
      */
     public courseClass: CourseClass|any; 
     
     /**
      * @description grade1
      */
     public grade1?: number; 
     
     /**
      * @description Status
      */
     public status?: number; 
     
     /**
      * @description grade2
      */
     public grade2?: number; 
     
     /**
      * @description gradeModified
      */
     public gradeModified?: Date; 
     
     /**
      * @description examGrade
      */
     public examGrade?: number; 
     
     /**
      * @description percentileRank
      */
     public percentileRank?: number; 
     
     /**
      * @description dateModified
      */
     public dateModified: Date; 
     
     /**
      * @description Βαθμός (στην κλίμακα)
      */
     public formattedGrade?: string; 
     
     /**
      * @description Course
      */
     public course?: Course|any; 
     
     /**
      * @description Το μάθημα έχει περαστεί
      */
     public isPassed?: number; 

}

export = StudentGrade;