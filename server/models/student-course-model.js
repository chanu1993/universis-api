import {DataObject} from "@themost/data/data-object";
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('StudentCourse')
/**
 * @class
 * @augments DataObject
 */
class StudentCourse extends DataObject {
 constructor() {
     super();
 }
}

module.exports = StudentCourse;
