import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class Rule extends DataObject {

     
     /**
      * @description Id
      */
     public id: number; 
     
     /**
      * @description The target object
      */
     public target: string; 
     
     /**
      * @description The type of the target object
      */
     public targetType: string; 
     
     /**
      * @description Refers to model
      */
     public refersTo: string; 
     
     /**
      * @description The rule additional type
      */
     public additionalType: string; 
     
     /**
      * @description The values to check
      */
     public checkValues?: string; 
     
     /**
      * @description The rule operator
      */
     public ruleOperator?: number; 
     
     /**
      * @description Value 1
      */
     public value1?: string; 
     
     /**
      * @description Value 2
      */
     public value2?: string; 
     
     /**
      * @description Value 3
      */
     public value3?: string; 
     
     /**
      * @description Value 4
      */
     public value4?: string; 
     
     /**
      * @description Value 5
      */
     public value5?: string; 
     
     /**
      * @description Value 6
      */
     public value6?: string; 
     
     /**
      * @description Value 7
      */
     public value7?: string; 
     
     /**
      * @description Value 8
      */
     public value8?: string; 
     
     /**
      * @description Value 9
      */
     public value9?: string; 
     
     /**
      * @description Value 10
      */
     public value10?: string; 
     
     /**
      * @description Value 11
      */
     public value11?: string; 
     
     /**
      * @description Value 12
      */
     public value12?: string; 
     
     /**
      * @description Value 13
      */
     public value13?: string; 
     
     /**
      * @description The rule expression
      */
     public ruleExpression?: string; 
     
     /**
      * @description The rule program specialty
      */
     public programSpecialty?: number; 

}

export = Rule;