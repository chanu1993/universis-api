import _ from 'lodash';
import util from "util";
import Rule from "./../models/rule-model";
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('CourseAttributeRule')
/**
 * @class
 * @augments Rule
 */
class CourseAttributeRule extends Rule {
    constructor() {
        super();
    }

    excludeCoursesDescription(callback) {
        const self = this;
      if (_.isNil(this.value7)) {
          return callback();
      }
        const values = this.value7.split(',');
        self.context.model('Course').where('id').in(values).select('id','displayCode','name').silent().all(function(err, result) {
            if (err) { return callback(err); }
            callback(null, result.map(function(x) {return util.format('(%s) %s', x.displayCode, x.name)}).join(', '));
        });
    }

    excludeCourseTypesDescription(callback) {
        const self = this;
        if (_.isNil(this.value8)) {
            return callback();
        }
        const values = this.value8.split(',');
        self.context.model('CourseType').where('id').in(values).select(['id','name']).silent().all(function(err, result) {
            if (err) { return callback(err); }
            callback(null, result.map(function(x) {return util.format('%s', x.name)}).join(', '));
        });
    }

    courseCategoriesDescription(callback) {
        const self = this;
        if (_.isNil(this.value9)) {
            return callback();
        }
        const values = this.value9.split(',');
        self.context.model('CourseCategory').where('id').in(values).select('id','name').silent().all(function(err, result) {
            if (err) { return callback(err); }
            callback(null, result.map(function(x) {return util.format('%s', x.name)}).join(', '));
        });
    }

    courseSectorsDescription(callback) {
        const self = this;
        if (_.isNil(this.value10)) {
            return callback();
        }
        const values = this.value10.split(',');
        self.context.model('CourseSector').where('id').in(values).select('id','name').silent().all(function(err, result) {
            if (err) { return callback(err); }
            callback(null, result.map(function(x) {return util.format('%s', x.name)}).join(', '));
        });
    }

    courseAreasDescription(callback) {
        const self = this;
        if (_.isNil(this.value11)) {
            return callback();
        }
        const values = this.value11.split(',');
        self.context.model('CourseArea').where('id').in(values).select('id','name').silent().all(function(err, result) {
            if (err) { return callback(err); }
            callback(null, result.map(function(x) {return util.format('%s', x.name)}).join(', '));
        });
    }

    ruleDescription(callback) {
        let s='';
        const self = this;
        self.excludeCoursesDescription(function (err, result) {
            if (err) {callback(err);}
            if (!_.isNil(result))
               s +=util.format("%s: %s\n",self.context.__("Exclude courses"),result);
            self.excludeCourseTypesDescription(function (err, result) {
                if (err) {callback(err);}
                if (!_.isNil(result))
                    s +=util.format("%s: %s\n",self.context.__("Exclude course types"),result);
                self.courseCategoriesDescription(function (err, result) {
                    if (err) {callback(err);}
                    if (!_.isNil(result))
                       s +=util.format("%s: %s\n",self.context.__("Course categories"),result);
                        self.courseSectorsDescription(function (err, result) {
                        if (err) {callback(err);}
                        if (!_.isNil(result))
                            s +=util.format("%s: %s\n",self.context.__("Course sectors"),result);
                        self.courseAreasDescription(function (err, result) {
                            if (err) {callback(err);}
                            if (!_.isNil(result))
                            s +=util.format("%s: %s\n",self.context.__("Course areas"),result);
                            callback(null, s);
                        });
                    });
                });
            });
        });
    }
}

module.exports = CourseAttributeRule;
