import util from 'util';
import {DataObject} from "@themost/data/data-object";
import {EdmMapping, EdmType} from "@themost/data/odata";

@EdmMapping.entityType('CourseClass')
/**
 * @class
 */
class CourseClass extends DataObject {
    constructor() {
        super();
    }

    courseOf(callback) {
        this.attrOf('course', callback);
    }

    label(callback) {
        const self = this;
        try {

            const fnCallback = function(err, result) {
                if (err) {
                    callback(err);
                }
                else if (result) {
                    callback(null, `[${result.displayCode}] ${result.name}`);
                }
                else {
                    callback();
                }
            };
            if (typeof self.course === 'string') {
                self.context.model('Course').where('id')
                    .equal(self.course)
                    .select('name','displayCode')
                    .silent()
                    .first(fnCallback)
            }
            else if (self.course && self.course.displayCode && self.course.name) {
                callback(null, util.format('[%s] %s', self.course.displayCode, self.course.name));
            }
            else {
                delete self.course;
                self.attr('course', fnCallback);
            }
        }
        catch (e) {
            callback(e);
        }
    }

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func("students",EdmType.CollectionOf("StudentCourseClass"))
    getStudents() {
        return this.context.model('StudentCourseClass').where('courseClass').equal(this.getId()).prepare();
    }

}

module.exports = CourseClass;