import util from 'util';
import {TraceUtils} from '@themost/common/utils';
import _ from 'lodash';
/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
    try {
        return callback();
    }
    catch (e) {
        callback(e);
    }
}

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function afterSave(event, callback) {
    try {
        return callback();
    } catch (e) {
        callback(e);
    }
}